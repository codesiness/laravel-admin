<tfoot>
    @if($mode === 'subtotal')
    <tr class="subtotal">
        @foreach($columns as $column)
            @if (floatval($column['value']) != 0)
            <td class="{{ $column['class'] }} subtotal">{!! $column['value'] !!}</td>
            @else
            <td class="{{ $column['class'] }}"></td>
            @endif
        @endforeach
    </tr>
    <tr class="total">
        @foreach($columns as $column)
            <td class="{{ $column['class'] }}">{!! $column['value'] !!}</td>
        @endforeach
    </tr>
    @else
    <tr class="total">
        @foreach($columns as $column)
            <td class="{{ $column['class'] }}">{!! $column['value'] !!}</td>
        @endforeach
    </tr>
    @endif

</tfoot>

<script>
    (function(){
        $('tfoot').find("td.subtotal").each(function(){
            var ix = $(this)[0].cellIndex+1;
            var val = Array.from($("tbody").first().find("tr td:nth-child("+ix+")").map(function(){
                val = parseFloat($(this).text().replaceAll(",",""));
                return isNaN(val)?0:val;
            })).reduce((a,b)=>a+b);
            $('tfoot').find("td.subtotal:nth-child("+ix+")").text(new Intl.NumberFormat('CNY').format(val));
        });
    })();
</script>


{{-- <tfoot>
    <tr>
        @foreach($columns as $column)
            <td class="{{ $column['class'] }}">{!! $column['value'] !!}</td>
        @endforeach
    </tr>
</tfoot>

 --}}
