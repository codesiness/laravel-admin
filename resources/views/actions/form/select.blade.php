<div class="form-group">
    <label>{{ $label }}</label>

    <select class="form-control {{$class}}" style="width: 100%;" name="{{$name}}" {!! $attributes !!} >
        @if ($groups)
            @foreach ($groups as $group)
            <optgroup label="{{$group[$groupLabel]}}">
                @foreach ($group[$groupKey] as $k => $v)
                    <option value="{{$v[$groupVal]}}" >{{$v[$groupLabel]}}</option>
                @endforeach
            </optgroup>
            @endforeach
        @else
            <option value=""></option>
            @foreach($options as $select => $option)
                <option value="{{$select}}">{{$option}}</option>
            @endforeach
        @endif
    </select>
    @include('admin::actions.form.help-block')
</div>

