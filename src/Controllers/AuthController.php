<?php

namespace Xn\Admin\Controllers;

use Xn\Admin\Facades\Admin;
use Xn\Admin\Form;
use Xn\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Xn\Admin\Controllers\Traits\AuthMethod;
use Xn\Admin\Controllers\Traits\Common;
use Xn\Admin\Controllers\Traits\LINENotify;
use Xn\Admin\Helper\XNCache;

class AuthController extends Controller
{

    use AuthMethod, LINENotify, Common;

    /**
     * @var string
     */
    protected $loginView = 'admin::login';

    /**
     * Show the login page.
     *
     * @return \Illuminate\Contracts\View\Factory|Redirect|\Illuminate\View\View
     */
    public function getLogin()
    {
        if ($this->guard()->check()) {
            return redirect($this->redirectPath());
        }

        $locales = XNCache::Locales();

        $timezones = XNCache::Timezones();

        # 切換語系
        $locale = App::getLocale();
        if (request()->has('locale')) {
            $locale = request()->get('locale');
            App::setLocale($locale);
        }
        # 切換時區
        $tmpTimezone = current(array_filter($timezones, function($item){
            return (isset($item['as_default'])&&($item['as_default']===1));
        }));

        $timezone = $tmpTimezone['timezone']??"";

        if (request()->has('timezone')) {
            $timezone = request()->get('timezone');
        }

        return view($this->loginView, compact('locales', 'locale', 'timezones', 'timezone'));
    }

    /**
     * Handle a login request.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function postLogin(Request $request)
    {
        $this->loginValidator($request->all())->validate();

        $credentials = $request->only([$this->username(), 'password']);
        $remember = $request->get('remember', false);

        if ($request->get('auth_otp', false) && $this->otpAuthValidate($request->only([$this->username(), 'auth_otp'])) === false) {
            return back()->withInput()->withErrors([
                'auth_otp' => __('auth.failed'),
            ]);
        }

        if ($this->guard()->attempt($credentials, $remember)) {

            $locale = request()->get('locale', config('app.locale'));
            session(['locale'=>$locale]);
            $timezone = request()->get('timezone', config('app.timezone'));
            session(['timezone'=>$timezone]);

            return $this->sendLoginResponse($request);
        }

        return back()->withInput()->withErrors([
            $this->username() => $this->getFailedLoginMessage(),
        ]);
    }

    /**
     * Get a validator for an incoming login request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function loginValidator(array $data)
    {
        $rules = [
            $this->username()   => 'required',
            'password'          => 'required',
        ];

        if (isset($data['username']) && !empty($data['username']))
        {
            $userModel = config('admin.database.users_model');
            $user = (new $userModel)->where('status', 1)->where($this->username(), $data['username'])->first();
            if($user) {
                switch ($user->auth_method) {
                    case 'otp':
                        $rules['auth_otp'] = 'required';
                        break;
                    case 'captcha':
                        $rules['auth_captcha'] = 'required|captcha';
                        break;

                }
            } else {
                // 防止BOT
                $rules['auth_captcha'] = 'required|captcha';
                $rules['fake_field'] = 'required';
                request()->session()->flash('auth_fail', __('admin.auth_fail'));
            }
        }

        return Validator::make($data, $rules);
    }

    /**
     * User logout.
     *
     * @return Redirect
     */
    public function getLogout(Request $request, string $message = "")
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        if ($message) {
            return redirect(config('admin.route.prefix')."/auth/login")->with('logout_msg', $message);
        }
        return redirect(config('admin.route.prefix'));
    }

    /**
     * User setting page.
     *
     * @param Content $content
     *
     * @return Content
     */
    public function getSetting(Content $content)
    {
        $form = $this->settingForm();
        $form->tools(
            function (Form\Tools $tools) {
                $tools->disableList();
                $tools->disableDelete();
                $tools->disableView();
            }
        );

        return $content
            ->title(trans('admin.user_setting'))
            ->body($form->edit(Admin::user()->id));
    }

    /**
     * Update user setting.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function putSetting()
    {
        return $this->settingForm()->update(Admin::user()->id);
    }

    /**
     * Model-form for user setting.
     *
     * @return Form
     */
    protected function settingForm()
    {
        $class = config('admin.database.users_model');

        $form = new Form(new $class());

        $form->display('username', trans('admin.username'));
        $form->text('name', trans('admin.name'))->rules('required');
        $form->image('avatar', trans('admin.avatar'));
        $form->lineNotify('line_notify_token', __('LINE Notify'))->attribute([
            'readonly'=>true,
            'data-callbackurl' => route('admin.line-notify.callback', ['username' => Admin::user()->username]),
            'data-cancelurl' => route('admin.line-notify.cancel', ['username' => Admin::user()->username]),
            'data-lineclientid' => config('admin.notify.line.client_id')
        ]);
        $form->passwordMeter('password', trans('admin.password'))->rules('confirmed|required');
        $form->password('password_confirmation', trans('admin.password_confirmation'))->rules('required')
            ->default(function ($form) {
                return $form->model()->password;
            });

        $form->radio('auth_method', trans('admin.auth_method'))->options(static::authMethod())->rules('required');
        $form->otpQrCode('google2fa_secret', __('OTP QRCode'))->setQrCodeLabel(Admin::user()->name);

        $form->setAction(admin_url('auth/setting'));

        $form->ignore(['password_confirmation']);

        $form->saving(function (Form $form) {
            if ($form->password && $form->model()->password != $form->password) {
                $form->password = Hash::make($form->password);
            }
        });

        $form->saved(function () {
            admin_toastr(trans('admin.update_succeeded'));

            return redirect(admin_url('auth/setting'));
        });

        return $form;
    }

    /**
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    protected function getFailedLoginMessage()
    {
        return Lang::has('auth.failed')
            ? trans('auth.failed')
            : 'These credentials do not match our records.';
    }

    /**
     * Get the post login redirect path.
     *
     * @return string
     */
    protected function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : config('admin.route.prefix');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        admin_toastr(trans('admin.login_successful'));

        $request->session()->regenerate();

        return redirect()->intended($this->redirectPath());
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    protected function username()
    {
        return 'username';
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Admin::guard();
    }
}
