<?php

namespace Xn\Admin\Controllers;

use Xn\Admin\Form;
use Xn\Admin\Grid;
use Xn\Admin\Show;

class TimeZoneController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    protected function title()
    {
        return trans('Time zones');
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $tzModel = config('admin.database.timezone_model');

        $grid = new Grid(new $tzModel());

        $grid->column('id', 'ID')->sortable();
        $grid->column('name', trans('admin.name'));
        $grid->column('timezone', trans('admin.timezone'));
        $grid->column('status', trans('admin.status'))->switch();
        $grid->column('sort_order', trans('admin.order'))->editable();

        $grid->column('created_at', trans('admin.created_at'));
        $grid->column('updated_at', trans('admin.updated_at'));

        $grid->model()->orderBy('id');

        $grid->tools(function (Grid\Tools $tools) {
            $tools->batch(function (Grid\Tools\BatchActions $actions) {
                $actions->disableDelete();
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        $tzModel = config('admin.database.timezone_model');

        $show = new Show($tzModel::findOrFail($id));

        $show->field('id', 'ID');
        $show->field('name', trans('admin.name'));
        $show->field('timezone', trans('admin.timezone'));
        $show->field('status', trans('admin.status'));
        $show->field('sort_order', trans('admin.order'));
        $show->field('created_at', trans('admin.created_at'));
        $show->field('updated_at', trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {

        $tzModel = config('admin.database.timezone_model');

        $form = new Form(new $tzModel());

        $form->display('id', 'ID');

        $form->text('name', trans('admin.name'))->rules('required');
        $form->text('timezone', trans('admin.timezone'))->rules('required');
        $form->switch('status', trans('admin.status'));
        $form->text('sort_order', trans('admin.order'))->inputmask(['alias' => 'integer'])->rules('required');

        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));

        return $form;
    }
}
