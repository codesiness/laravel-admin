<?php

namespace Xn\Admin\Actions;

use Illuminate\Database\Eloquent\Model;

/**
 * 提供在Grid Action可以彈窗編輯
 *
 * $grid->actions(function ($actions) {
 *      $actions->append(new Replicate);
 * });
 */
abstract class RowModal extends RowAction
{

    /**
     * @var Interactor\Interactor
     */
    protected $interactor;

    /**
     * Action constructor.
     */
    public function __construct()
    {
        $this->initInteractor();
    }

    /**
     * @throws \Exception
     */
    protected function initInteractor()
    {
        if ($hasForm = method_exists($this, 'form')) {
            $this->interactor = new \Xn\Admin\Actions\Interactor\Form($this);
        }

        if ($hasDialog = method_exists($this, 'dialog')) {
            $this->interactor = new \Xn\Admin\Actions\Interactor\Dialog($this);
        }

        if ($hasForm && $hasDialog) {
            throw new \Exception('Can only define one of the methods in `form` and `dialog`');
        }
    }

    public function handle(Model $model)
    {
    }

    /**
     * Render row action.
     *
     * @return string
     */
    public function render()
    {
        $this->addScript();

        $modalId = '';

        if ($this->interactor instanceof \Xn\Admin\Actions\Interactor\Form) {
            $modalId = $this->interactor->getModalId();
        }

        return sprintf(
            "<a data-_key='%s' href='javascript:void(0);' class='%s' %s>%s</a>",
            $this->getKey(),
            $this->getElementClass(),
            $modalId ? "modal='{$modalId}'" : '',
            $this->name()
        );
    }
}
