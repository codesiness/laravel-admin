<?php

namespace Xn\Admin\Actions;

use Xn\Admin\Admin;
use Xn\Admin\Form\Field;

/**
 * @method $this                success($title, $text = '', $options = [])
 * @method $this                error($title, $text = '', $options = [])
 * @method $this                warning($title, $text = '', $options = [])
 * @method $this                info($title, $text = '', $options = [])
 * @method $this                question($title, $text = '', $options = [])
 * @method $this                confirm($title, $text = '', $options = [])
 * @method Field\Text           text($column, $label = '')
 * @method Field\Email          email($column, $label = '')
 * @method Field\Integer        integer($column, $label = '')
 * @method Field\Ip             ip($column, $label = '')
 * @method Field\Url            url($column, $label = '')
 * @method Field\Password       password($column, $label = '')
 * @method Field\Mobile         mobile($column, $label = '')
 * @method Field\Textarea       textarea($column, $label = '')
 * @method Field\Select         select($column, $label = '')
 * @method Field\Selectize      selectize($column, $label = '')
 * @method Field\MultipleSelect multipleSelect($column, $label = '')
 * @method Field\Checkbox       checkbox($column, $label = '')
 * @method Field\Radio          radio($column, $label = '')
 * @method Field\File           file($column, $label = '')
 * @method Field\Image          image($column, $label = '')
 * @method Field\MultipleFile   multipleFile($column, $label = '')
 * @method Field\MultipleImage  multipleImage($column, $label = '')
 * @method Field\Date           date($column, $label = '')
 * @method Field\Datetime       datetime($column, $label = '')
 * @method Field\Time           time($column, $label = '')
 * @method Field\Hidden         hidden($column, $label = '')
 * @method $this                modalLarge()
 * @method $this                modalSmall()
 */
abstract class ActionLayer extends Action
{

    protected $size = ['800px', '600px'];

    /**
     * @var string
     */
    protected $renderable;

    /**
     * Action constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->renderable = get_called_class();
    }

    public function form()
    {
        $widget = $this->make();

        if (is_a($widget, \Xn\Admin\Widgets\Form::class)) {
            $this->handleScript();
            $formID = str_replace(".", '', $this->selector);
            $widget->attribute('id', $formID);
            $widget->action($this->getHandleRoute());
        }

        Admin::html($widget->render());

        return view('admin::layer');
    }

    protected function handleScript() {
        $parameters = json_encode($this->parameters());
        $formID = str_replace(".", '', $this->selector);
        $script = <<<SCRIPT

(function ($) {
    $('body').on('submit', '#{$formID}', function (e) {
        var formDataArray = $(this).serializeArray();
        var data = {};
        formDataArray.forEach(function(input) {
            data[input.name] = input.value;
        });
        var target = $(this);
        Object.assign(data, {$parameters});
        {$this->actionScript()}
        {$this->buildActionPromise()}
        {$this->handleActionPromise()}
        return false;
    });
})(jQuery);

SCRIPT;

        Admin::script($script);
    }

    /**
     * @param int $multiple
     *
     * @return string
     */
    protected function getLoadUrl()
    {
        $renderable = str_replace('\\', '_', $this->renderable);

        return route('admin.handle-renderable', compact('renderable'));
    }

    /**
     * @return Form
     */
    abstract public function make();

    /**
     * @return mixed
     */
    protected function addScript()
    {
        $size = json_encode($this->size);
        $script = <<<SCRIPT

(function ($) {
    $('body').off('click', '{$this->selector}').on('click', '{$this->selector}', function() {
        layer.open({
            type: 2,
            area: {$size},
            title: '{$this->name()}',
            shade: 0, // 遮罩透明度
            shadeClose: true, // 点击遮罩区域，关闭弹层
            maxmin: true, // 允许全屏最小化
            anim: 0, // 0-6 的动画形式，-1 不开启
            offset: 'auto',
            zIndex: layer.zIndex,
            content: '{$this->getLoadUrl()}',
            success: function(layero, index){
                // layer.iframeAuto(index); // 让 iframe 高度自适应
                layer.setTop(layero);
                layer.escIndex = layer.escIndex || [];
                layer.escIndex.unshift(index);
                layero.on('mousedown', function(){
                    var _index = layer.escIndex.indexOf(index);
                    if(_index !== -1){
                        layer.escIndex.splice(_index, 1);
                    }
                    layer.escIndex.unshift(index);
                });
            },
            end: function(){
                if(typeof layer.escIndex === 'object'){
                    layer.escIndex.splice(0, 1);
                }
            },
        });
    });
})(jQuery);

SCRIPT;

        Admin::script($script);
    }

    /**
     * @return mixed
     */
    public function render()
    {

        $this->addScript();

        $content = $this->html();

        if ($content && $this->interactor instanceof Interactor\Form) {
            return $this->interactor->addElementAttr($content, $this->selector);
        }

        return $this->html();
    }
}
