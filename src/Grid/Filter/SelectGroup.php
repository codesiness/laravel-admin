<?php

namespace Xn\Admin\Grid\Filter;

use Xn\Admin\Grid\Filter\Presenter\SelectOptGroup;

class SelectGroup extends AbstractFilter
{

    protected $query = 'where';

    protected $fieldName = 'selectGroup';


    public function __construct($column, $label = '')
    {
        parent::__construct($column, $label);

        return $this->setPresenter(new SelectOptGroup());
    }

}
