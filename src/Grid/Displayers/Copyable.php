<?php

namespace Xn\Admin\Grid\Displayers;

use Xn\Admin\Facades\Admin;

/**
 * Class Copyable.
 *
 * @see https://codepen.io/shaikmaqsood/pen/XmydxJ
 */
class Copyable extends AbstractDisplayer
{
    protected function addScript()
    {
        $script = <<<SCRIPT
$('#{$this->grid->tableID}').on('click','.grid-column-copyable',(function (e) {
    var content = $(this).data('content');

    var temp = $('<textarea>');

    $("body").append(temp);
    temp.val(content).select();
    document.execCommand("copy");
    temp.remove();

    $(this).tooltip('show');
}));
SCRIPT;

        Admin::script($script);
    }

    public function display()
    {
        $this->addScript();

        $content =  $this->getColumn()->getOriginal();
        $label = $this->getValue();

        if ($func = func_get_args()[0]??null) {

            $func = $func->bindTo($this->row);

            $content = call_user_func($func, $this->getColumn()->getOriginal());

            if(is_array($content))
            {
                list($label, $content) = $content;
            }
        }
        $copyTitle = __('Copied!');
        return <<<HTML
<a href="javascript:void(0);" class="grid-column-copyable text-muted" data-content="{$content}" title="{$copyTitle}" data-placement="bottom">
    <i class="far fa-copy"></i>
</a>&nbsp;{$label}
HTML;
    }
}
