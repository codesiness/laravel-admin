<?php

namespace Xn\Admin\Form\Field;

use Xn\Admin\Form\Field;

class Editor extends Field
{
    protected static $js = [
        '/vendor/laravel-admin/ckeditor/ckeditor.js',
        '/vendor/laravel-admin/ckeditor/adapters/jquery.js',
    ];

    public function render()
    {
        $options = json_encode(config('admin.extensions.editor.options', []));

        $this->script = "$('textarea.{$this->getElementClass()[0]}').ckeditor($options);";

        return parent::render();
    }
}
